# Ansible playbooks for lorainelab.org upgrade  #
This repository contains playbooks for deploying infrastructure and upgrade for the lorainelab.org. 
There are two playbooks.

1. aws.yml - Creates the infrastructure if not present.
2. main.yml - Sets up the wordpress server.
3. inventory.ini - This contains the servers public IP and the Domain to be used for http connections.

***
## Requirements for Running the playbooks
1. Configuration setup needed
   * Python 3 or above
   * Ansible 2.9 or above
    
    
2. Add the following files to the files folder of the playbook heirarchy
    * SSL Certificate
    * SSL Certificate Key
    * Chain Certificate
    * Loraine labs Site sql backup from Backup S3 bucket
    * Loraine labs wordpress backup tar.gz

***
## Data to be added to execute playbooks
* Fill out the configurations in the group_vars/common.yml
* Fill out the configurations in the group_vars/secrets.yml

Note that the mysql root password here is not required for mysql changes due to the use of the socket file for root login


***
## Playbook Execution
The Playbooks can be executed using the below commands.

1. Infrastructure provisioning

`ansible-playbook aws.yml`

2. Site deployment

`ansible-playbook main.yml -i inventory.ini`
***
## Post execution Tasks
* ### Update Wordpress version to the latest 
    * Login into Wordpres by navigating to http://<domain>/wp-login.php Login with admin user and passwords
    * Click on the Updates section and click update wordpress to latest version
    * Click on the Update Now button (This will update the wordpress package to latest)
    * Click on Plugins and Update Each individual plugin By clicking Update now button, on each plugin information list item.
* ### Enable SSL for wordpress
    * Click the Reload over SSL button in the update plugins view opened earlier while updating the plugins.
    * Update ADMIN email or Click `The email is correct` button.
    * Click the `Go ahead, activate SSL!` button on the top of the page.
    * Review the messages and dismiss them if not applicable
    * Go to Wordpress Settings(left had side navigation pane) click SSL. In the SSL Settings, Enable `Enable 301 .htaccess redirect`
* ### Update Themes
    * Click on the Dashboard button in the Left pane. Click updates
    * Select all themes and update themes.
    
***
# Notes on AWS Infrastructure
* EC2 Config to be used
    * Instance Type: t2.micro or t4g.micro
    * Instance Volume Size: 8GB
    * Amazon Machine Image(AMI): Ubuntu v20


# Notes on .htaccess file
This file being hidden sometime permissions get changed, resulting in connection failures while setting up the SSL certificate.
Possible solution is to run `sudo chmod 777 .htaccess` The file is located in /var/www/html/ folder

***

    

### Contact ###
**Chester 